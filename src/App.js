import React, { Component } from "react";
import Header from './components/Layout/Header'
import HeaderHook from './components/Layout/HeaderHook'
import MenuLeft from './components/Layout/MenuLeft'
import Footer from './components/Layout/Footer'
import {
	withRouter
} from 'react-router-dom';
import {MyProvider} from "./components/MyProvider";
import {HeaderProvider} from './components/HeaderContext';

class App extends Component {

	// Kiểm tra xem có đang ở Component Account ko để Ẩn/Hiện MenuLeft
	showMenuLeft(propsValue) {
		// console.log(propsValue.location.pathname)
		let url = propsValue.location.pathname

		if (url.indexOf('account') !== -1 || url.indexOf('cart') !== -1) {
			return (
				<div>
					{this.props.children}
				</div>
			)
		} else {
			return (
				<div>
					<MenuLeft />
					{this.props.children}
				</div>
			)
		}
	}

	render() {
		return (
			<HeaderProvider>
				<div className="App">
					{/* <Header /> */}
					<HeaderHook />
					<section>
						<div className='container'>
							<div className='row'>
								{this.showMenuLeft(this.props)}
							</div>
						</div>
					</section>
					<Footer />
				</div>
			</HeaderProvider>
		);
	}
}

export default withRouter(App);

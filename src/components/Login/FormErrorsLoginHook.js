import React from "react";

function FormErrorsLoginHook(props) {

    function showFormErrorsLogin() {
        let { formErrorsLogin } = props
        console.log(formErrorsLogin)
        if (Object.values(formErrorsLogin).length > 0) {
            return Object.values(formErrorsLogin).map((value, index) => {
                return (
                    <p
                        key={index}
                    >
                        {value}
                    </p>
                )
            })
        }
    }

    return (
        <div className="FormErrorsLogin">
            {showFormErrorsLogin()}
        </div>
    )
}

export default FormErrorsLoginHook;
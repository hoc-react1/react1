import React, { Component } from "react";
import FormErrorsSignUp from "./FormErrorsSignUp";
import FormErrorsLogin from "./FormErrorsLogin";
import axios from "axios";
import { MyContext } from "../MyProvider";

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            passwordLogin: '',
            email: '',
            emailLogin: '',
            password: '',
            phone: '',
            address: '',
            level: '0',
            levelLogin: '0',
            file: {},
            avatar: '',
            formErrors: {},
            formErrorsLogin: {},
            fileType: ['png', 'jpg', 'jpeg'],
            isLogin: '',
            allValid: ''
        }

        this.handleInput = this.handleInput.bind(this)
        this.handleSubmitSignUp = this.handleSubmitSignUp.bind(this)
        this.handleSubmitLogin = this.handleSubmitLogin.bind(this)
    }

    handleInput = (e) => {
        let target = e.target;
        let nameInput = target.name;
        let value = target.value;
        const file = e.target.files;

        this.setState({
            [nameInput]: (target.type === 'file' ? target.files[0] : value)
        })

        console.log(file)
        if (file) {
            let reader = new FileReader();

            reader.onload = (e) => [
                this.setState({
                    avatar: e.target.result
                })
            ]
            reader.readAsDataURL(file[0])
        }
        // console.log(target.files)
    }

    handleSubmitSignUp = (e) => {
        e.preventDefault()

        let allValid = true;
        let { name, email, password, phone, address, level, file, avatar } = this.state;
        let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        let errorSubmit = this.state.formErrors;

        if (!name) {
            allValid = false;
            errorSubmit.name = 'Vui long nhap ten'
        } else {
            delete errorSubmit.name
        }

        if (!(regex.test(email))) {
            allValid = false;
            errorSubmit.email = 'Vui long nhap email'
        } else {
            delete errorSubmit.email
        }

        if (!password) {
            allValid = false;
            errorSubmit.password = 'Vui long nhap mat khau'
        } else {
            delete errorSubmit.password
        }

        console.log(typeof phone)
        console.log(phone !== '')
        console.log(phone.length >= 10)
        if (!(phone !== '' && phone.length >= 10)) {
            allValid = false;
            errorSubmit.phone = 'Vui long nhap so dien thoai'
            console.log('123')
        } else {
            delete errorSubmit.phone
        }

        if (!address) {
            allValid = false;
            errorSubmit.address = 'Vui long nhap dia chi'
        } else {
            delete errorSubmit.address
        }

        if (!level) {
            allValid = false;
            errorSubmit.level = 'Vui long nhap level'
        } else {
            delete errorSubmit.level
        }

        console.log(file)
        if (file.name) {
            var fileFormat = file.name.split('.')[1]
            var fileSize = file.size
        }

        let isPhoto = this.state.fileType.some((value, index) => {
            return value === fileFormat;
        })
        // console.log(isPhoto)
        // console.log(fileSize)
        if (!(isPhoto && (fileSize < 1024 * 1024))) {
            allValid = false;
            errorSubmit.fileError = 'Chon file anh va co kich thuoc nho hon 1MB'
            // console.log('Chon file anh va co kich thuoc nho lon 1MB')
        } else {
            delete errorSubmit.file
        }


        // console.log(allValid)
        // console.log(errorSubmit)
        if (!allValid) {
            this.setState({
                formErrors: errorSubmit
            })
        } else {
            this.setState({
                formErrors: {},
                allValid: true
            })
            axios({
                method: 'POST',
                url: 'http://localhost/laravel/laravel/public/api/register',
                data: {
                    name: name,
                    email: email,
                    password: password,
                    phone: phone,
                    address: address,
                    level: level,
                    file: file,
                    avatar: avatar
                }
            }).then(res => {
                console.log(res)
            }).catch(err => {
                console.log(err)
            })
        }
    }

    handleSubmitLogin = (e) => {
        e.preventDefault();

        let allValid = true;
        let { passwordLogin, emailLogin, levelLogin } = this.state;
        let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        let errorSubmit = this.state.formErrorsLogin;

        if (!(regex.test(emailLogin))) {
            allValid = false;
            errorSubmit.emailLogin = 'Vui long nhap email'
        } else {
            delete errorSubmit.emailLogin
        }

        if (!passwordLogin) {
            allValid = false;
            errorSubmit.passwordLogin = 'Vui long nhap mat khau'
            console.log('123')
        } else {
            delete errorSubmit.passwordLogin
        }

        if (!levelLogin) {
            allValid = false;
            errorSubmit.levelLogin = 'Vui long nhap level'
        } else {
            delete errorSubmit.levelLogin
        }

        console.log(errorSubmit)
        if (!allValid) {
            this.setState({
                formErrorsLogin: errorSubmit
            })
        } else {
            this.setState({
                formErrorsLogin: {},
                allValid: true
            })

            axios({
                method: 'POST',
                url: 'http://localhost/laravel/laravel/public/api/login',
                data: {
                    email: emailLogin,
                    password: passwordLogin,
                    level: levelLogin,
                }
            }).then(res => {
                console.log(res)
                console.log(res.data.response)
                if (res.data.response === 'success') {
                    let isLogin = true;
                    let isLoginJson = JSON.stringify(isLogin)
                    localStorage.setItem('isLogin', isLoginJson)
                    this.setState({
                        isLogin : true
                    })

                    let userData = {}
                    userData['token'] = res.data.success.token
                    userData['id_user'] = res.data.Auth.id
                    userData['name_user'] = res.data.Auth.name
                    userData['image_user'] = res.data.Auth.avatar
                    userData['email_user'] = res.data.Auth.email
                    userData['phone_user'] = res.data.Auth.phone
                    userData['address_user'] = res.data.Auth.address
                    userData['password_user'] = passwordLogin
                    let userDataJson = JSON.stringify(userData)
                    localStorage.setItem('userData', userDataJson)

                    this.props.history.goBack();
                }
            }).catch(err => {
                console.log(err)
            })
        }
    }

    changeLoginStatus(context) {
        let {allValid} = this.state
        if (allValid) {
            context.handleIsLogin(true)
            // Chuyển allValid về false nếu ko setIsLogin luôn được set = true (luôn hiện Logout)
            this.setState({
                allValid : false
            })
            console.log(context)
        }
    }

    render() {
        return (
            <section id="form">{/*form*/}
                <div className="container">
                    <div className="row">
                        <div className="col-sm-4 col-sm-offset-1">
                            <div className="login-form">{/*login form*/}
                                <h2>Login to your account</h2>
                                <MyContext.Consumer>
                                    {(context) => (
                                        <React.Fragment>
                                            <form onSubmit={this.handleSubmitLogin} encType="multipart/form-data">
                                                <input type="text" name='emailLogin' onChange={this.handleInput} placeholder="Email Address" />
                                                <input type="text" name='passwordLogin' onChange={this.handleInput} placeholder="Password" />
                                                <input type="number" name="level" onChange={this.handleInput} value={this.state.levelLogin} placeholder="Level (1: admin, 0: member)" />
                                                <span>
                                                    <input type="checkbox" className="checkbox" />
                                                    Keep me signed in
                                                </span>
                                                <button type="submit" className="btn btn-default">Login</button>
                                            </form>
                                            {this.changeLoginStatus(context)}
                                        </React.Fragment>
                                    )}
                                </MyContext.Consumer>
                                <FormErrorsLogin formErrorsLogin={this.state.formErrorsLogin} />
                            </div>{/*/login form*/}
                        </div>
                        <div className="col-sm-1">
                            <h2 className="or">OR</h2>
                        </div>
                        <div className="col-sm-4">
                            <div className="signup-form">{/*sign up form*/}
                                <h2>New User Signup!</h2>
                                <form onSubmit={this.handleSubmitSignUp}>
                                    <input type="text" name="name" onChange={this.handleInput} placeholder="Name" />
                                    <input type="text" name="email" onChange={this.handleInput} placeholder="Email Address" />
                                    <input type="password" name="password" onChange={this.handleInput} placeholder="Password" />
                                    <input type="number" name="phone" onChange={this.handleInput} placeholder="Phone" />
                                    <input type="text" name="address" onChange={this.handleInput} placeholder="Address" />
                                    <input type="number" name="level" onChange={this.handleInput} value={this.state.level} placeholder="Level (1: admin, 0: member)" />
                                    <input type="file" name="file" onChange={this.handleInput} />
                                    <button type="submit" className="btn btn-default">Signup</button>
                                </form>
                            </div>{/*/sign up form*/}<br />
                            <FormErrorsSignUp formErrorsSignUp={this.state.formErrors} />
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
export default Login;
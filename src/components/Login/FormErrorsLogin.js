import React, { Component } from "react";

class FormErrorsLogin extends Component {
	// constructor(props) {
	// 	super(props)
	// }

    showFormErrorsLogin() {
        let formErrorsLogin = this.props.formErrorsLogin
        if (Object.values(formErrorsLogin).length > 0) {
            return Object.values(formErrorsLogin).map((value, index) => {
                return (
                    <p
                        key = {index}
                    >
                        {value}
                    </p>
                )
            })
        }
    }

	render() {
		return (
			<div className="FormErrorsLogin">
				{this.showFormErrorsLogin()}
			</div>
		);
	}
}

export default FormErrorsLogin;

import React, { useState, useEffect, useContext } from "react";
import FormErrorsSignUpHook from "./FormErrorsSignUpHook";
import FormErrorsLogin from "./FormErrorsLoginHook";
import axios from "axios";
import {useHistory} from 'react-router-dom';
import { Context } from "../HeaderContext";

function Login() {
    const headerContext = useContext(Context)

    const history = useHistory();
    const [inputs, setInputs] = useState({
        level: '0',
        levelLogin: '0'
    })

    const [allValid, setAllValid] = useState('')
    const [avatar, setAvatar] = useState('')
    const [formErrorsSignUp, setFormErrorsSignUp] = useState({})
    const [formErrorsLogin, setFormErrorsLogin] = useState({})
    const [fileType, setFileType] = useState(['png', 'jpg', 'jpeg'])
    const [isLogin, setIsLogin] = useState('')
    const [isSignUpSucc, setIsSignUpSucc] = useState('')
    const [isLoginSucc, setIsLoginSucc] = useState('')

    const handleInput = (e) => {
        let nameInput = e.target.name;
        let value = e.target.value;
        if (e.target.type == 'file') {
            value = e.target.files[0]
        }

        setInputs(state => ({
            ...state, [nameInput]: value
        }))

        if (e.target.files) {
            let reader = new FileReader();

            reader.onload = (e) => [
                setAvatar(e.target.result)
            ]
            reader.readAsDataURL(e.target.files[0])
        }
    }

    const handleSubmitSignUp = (e) => {
        e.preventDefault()

        let allValid = true;
        // let { name, email, password, phone, address, level, file, avatar } = this.state;
        let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        let errorSubmit = {};

        if (!inputs.username) {
            allValid = false;
            errorSubmit.username = 'Vui long nhap ten'
        } else {
            delete errorSubmit.username
        }

        if (!(regex.test(inputs.email))) {
            allValid = false;
            errorSubmit.email = 'Vui long nhap email'
        } else {
            delete errorSubmit.email
        }

        if (!inputs.password) {
            allValid = false;
            errorSubmit.password = 'Vui long nhap mat khau'
        } else {
            delete errorSubmit.password
        }

        // console.log(typeof phone)
        // console.log(phone !== '')
        // console.log(phone.length >= 10)

        // console.log(inputs.phone)
        if (inputs.phone) {
            if (!(inputs.phone !== '' && inputs.phone.length >= 10)) {
                allValid = false;
                errorSubmit.phone = 'Vui long nhap so dien thoai'
            } else {
                delete errorSubmit.phone
            }
        } else {
            allValid = false;
            errorSubmit.phone = 'Vui long nhap so dien thoai'
        }

        if (!inputs.address) {
            allValid = false;
            errorSubmit.address = 'Vui long nhap dia chi'
        } else {
            delete errorSubmit.address
        }

        if (!inputs.level) {
            allValid = false;
            errorSubmit.level = 'Vui long nhap level'
        } else {
            delete errorSubmit.level
        }

        // console.log(inputs.file)
        if (inputs.file) {
            if (inputs.file.name) {
                var fileFormat = inputs.file.name.split('.')[1]
                var fileSize = inputs.file.size
            }

            let isPhoto = fileType.some((value, index) => {
                return value === fileFormat;
            })

            if (!(isPhoto && (fileSize < 1024 * 1024))) {
                allValid = false;
                errorSubmit.fileError = 'Chon file anh va co kich thuoc nho hon 1MB'
            } else {
                console.log('delete')
                delete errorSubmit.fileError
            }
        } else {
            allValid = false
            errorSubmit.fileError = 'Chon file anh va co kich thuoc nho hon 1MB'
        }

        if (!allValid) {
            console.log(errorSubmit)
            setFormErrorsSignUp(errorSubmit)
            // setFormErrorsSignUp(errorSubmit)
        } else {
            setFormErrorsSignUp({})
            setIsSignUpSucc('Đăng ký tài khoản thành công')
            axios({
                method: 'POST',
                url: 'http://localhost/laravel/laravel/public/api/register',
                data: {
                    name: inputs.username,
                    email: inputs.email,
                    password: inputs.password,
                    phone: inputs.phone,
                    address: inputs.address,
                    level: inputs.level,
                    file: inputs.file,
                    avatar: avatar
                }
            }).then(res => {
                console.log(res)
            }).catch(err => {
                console.log(err)
            })
        }
    }

    const handleSubmitLogin = (e) => {
        e.preventDefault();

        let allValid = true;
        let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        let errorSubmit = formErrorsLogin;

        if (!(regex.test(inputs.emailLogin))) {
            allValid = false;
            errorSubmit.emailLogin = 'Vui long nhap email'
        } else {
            delete errorSubmit.emailLogin
        }

        if (!inputs.passwordLogin) {
            allValid = false;
            errorSubmit.passwordLogin = 'Vui long nhap mat khau'
        } else {
            delete errorSubmit.passwordLogin
        }

        if (!inputs.levelLogin) {
            allValid = false;
            errorSubmit.levelLogin = 'Vui long nhap level'
        } else {
            delete errorSubmit.levelLogin
        }

        console.log(errorSubmit)
        if (!allValid) {
            // setFormErrorsLogin({...errorSubmit})
            setFormErrorsLogin(errorSubmit)
        } else {
            setFormErrorsLogin({})
            setAllValid(true)
            console.log(inputs.emailLogin)
            console.log(inputs.passwordLogin)
            axios({
                method: 'POST',
                url: 'http://localhost/laravel/laravel/public/api/login',
                data: {
                    email: inputs.emailLogin,
                    password: inputs.passwordLogin,
                    level: inputs.levelLogin,
                }
            }).then(res => {
                console.log(res)
                console.log(res.data.response)
                if (res.data.response === 'success') {
                    let isLogin = true;
                    let isLoginJson = JSON.stringify(isLogin)
                    localStorage.setItem('isLogin', isLoginJson)
                    setIsLogin(true)

                    let userData = {}
                    userData['token'] = res.data.success.token
                    userData['id_user'] = res.data.Auth.id
                    userData['name_user'] = res.data.Auth.name
                    userData['image_user'] = res.data.Auth.avatar
                    userData['email_user'] = res.data.Auth.email
                    userData['phone_user'] = res.data.Auth.phone
                    userData['address_user'] = res.data.Auth.address
                    userData['password_user'] = inputs.passwordLogin
                    let userDataJson = JSON.stringify(userData)
                    localStorage.setItem('userData', userDataJson)

                    history.goBack();
                    alert('Đăng nhập thành công!')
                }
            }).catch(err => {
                console.log(err)
            })
        }
    }

    const changeLoginStatus = () => {
        console.log('allValid', allValid)
        if (allValid) {
            console.log(headerContext)
            headerContext.setIsLogin(true)
        }
    }

    return (
        <section id="form">{/*form*/}
            <div className="container">
                <div className="row">
                    <div className="col-sm-4 col-sm-offset-1">
                        <div className="login-form">{/*login form*/}
                            <h2>Login to your account</h2>
                            <form onSubmit = {handleSubmitLogin} encType="multipart/form-data">
                                <input type="text" name='emailLogin' onChange={handleInput} placeholder="Email Address" />
                                <input type="text" name='passwordLogin' onChange={handleInput} placeholder="Password" />
                                <input type="number" name="levelLogin" onChange={handleInput} value={inputs.levelLogin} placeholder="Level (1: admin, 0: member)" />
                                <span>
                                    <input type="checkbox" className="checkbox" />
                                    Keep me signed in
                                </span>
                                <button type="submit" className="btn btn-default">Login</button>
                            </form>
                            {changeLoginStatus()}
                            <p className = 'isLoginSucc'>{isLoginSucc}</p>
                            <FormErrorsLogin formErrorsLogin={formErrorsLogin} />
                        </div>{/*/login form*/}
                    </div>
                    <div className="col-sm-1">
                        <h2 className="or">OR</h2>
                    </div>
                    <div className="col-sm-4">
                        <div className="signup-form">{/*sign up form*/}
                            <h2>New User Signup!</h2>
                            <form onSubmit={handleSubmitSignUp} encType="multipart/form-data">
                                <input type="text" name="username" onChange={handleInput} placeholder="Name" />
                                <input type="text" name="email" onChange={handleInput} placeholder="Email Address" />
                                <input type="password" name="password" onChange={handleInput} placeholder="Password" />
                                <input type="number" name="phone" onChange={handleInput} placeholder="Phone" />
                                <input type="text" name="address" onChange={handleInput} placeholder="Address" />
                                <input type="number" name="level" onChange={handleInput} value={inputs.level} placeholder="Level (1: admin, 0: member)" />
                                <input type="file" name="file" onChange={handleInput} />
                                <button type="submit" className="btn btn-default">Signup</button>
                            </form>
                            <p className = 'isSignUpSucc'>{isSignUpSucc}</p>
                        </div>{/*/sign up form*/}<br />
                        <FormErrorsSignUpHook formErrorsSignUp={formErrorsSignUp} />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Login;
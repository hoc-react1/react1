import React from "react";

function FormErrorsSignUpHook(props) {
    
    function showFormErrorsSignUp() {
        let {formErrorsSignUp} = props
       
        if (Object.keys(formErrorsSignUp).length > 0) {
            console.log(formErrorsSignUp)

            return Object.keys(formErrorsSignUp).map((key, index) => {
                return (
                    <p
                        key = {index}
                    >
                        {formErrorsSignUp[key]}
                    </p>
                )
            })
        }
    }

    return (
        <div className="FormErrorsSignUp">
            {showFormErrorsSignUp()}
        </div>
    )
}

export default FormErrorsSignUpHook;
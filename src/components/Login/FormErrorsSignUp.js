import React, { Component } from "react";

class FormErrorsSignUp extends Component {
	// constructor(props) {
	// 	super(props)
	// }

    showFormErrorsSignUp() {
        let formErrorsSignUp = this.props.formErrorsSignUp  
        
        if (Object.values(formErrorsSignUp).length > 0) {
            return Object.values(formErrorsSignUp).map((value, index) => {
                return (
                    <p
                        key = {index}
                    >
                        {value}
                    </p>
                )
            })
        }
    }

	render() {
		return (
			<div className="FormErrorsSignUp">
				{this.showFormErrorsSignUp()}
			</div>
		);
	}
}

export default FormErrorsSignUp;

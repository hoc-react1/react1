import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

class Products extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataProducts: {},
            accessToken: ''
        }
    }

    componentDidMount() {
        let userDataJson = localStorage.getItem('userData')
        if (userDataJson) {
            let userData = JSON.parse(userDataJson)
            let accessToken = userData.token
            axios({
                method: 'GET',
                url: 'http://localhost/laravel/laravel/public/api/user/my-product',
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }).then(res => {
                // console.log(res)
                // console.log(res.data.data)
                var dataProducts = res.data.data

                this.setState({
                    dataProducts: dataProducts,
                    accessToken: accessToken
                })
            })
        }
    }

    showMyProducts() {
        let { dataProducts } = this.state
        return Object.values(dataProducts).map((value, index) => {
            // console.log(value)
            let imgJson = value.image
            let imgProduct = JSON.parse(imgJson)[0]
            // console.log(imgProduct)
            // console.log(imgJson)
            return (
                <tr key={index}>
                    <td>{value.id}</td>
                    <td>{value.name}</td>
                    <td>
                        <img
                            className='acc-product-img'
                            src={"http://localhost/laravel/laravel/public/upload/user/product/8/" + imgProduct} alt=""
                        >
                        </img>
                    </td>
                    <td>{value.price}$</td>
                    <td>
                        <Link to={'/account/product/edit/' + value.id} type="button" class="btn btn-warning">Edit</Link>&nbsp;&nbsp;
                        <button onClick={() => { this.deleteProduct(value) }} type="button" class="btn btn-success">Delete</button>
                    </td>
                </tr>
            )
        })
    }

    deleteProduct(value) {
        console.log(value)
        // const formData = new FormData();
        // formData.append('id_user', value.id_user);
        // formData.append('name', value.name);
        // formData.append('price', value.price);
        // formData.append('category', value.id_category);
        // formData.append('brand', value.id_brand);
        // formData.append('status', value.status);
        // formData.append('sale', value.sale);
        // formData.append('company', value.company_profile);
        // formData.append('detail', value.detail);
        // Object.keys(file).map((item, i) => {
        //     formData.append('file[]', file[item]);
        //     console.log(item)
        // })
        // avatarCheckbox.map((item, i) => {
        //     formData.append('avatarCheckbox[]', item);
        //     console.log(item)
        // })
        axios({
            method: 'GET',
            url: 'http://localhost/laravel/laravel/public/api/user/delete-product/' + value.id,
            // data: formData,
            headers: {
                'Authorization': 'Bearer ' + this.state.accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
            }
        }).then(res => {
            console.log(res)
            
            this.setState({
                dataProducts: res.data.data
            })
        }).catch(err => {
            console.log(err)
        })
    }

    render() {
        return (
            <div className='col-sm-8'>
                <div className='product-list'>
                    <table class="table table-striped table-inverse table-responsive">
                        <thead class="thead-inverse">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.showMyProducts()}
                        </tbody>
                    </table>
                </div>

                <div className='add-product-btn'>
                    <Link to='/account/product/add' type="button" class="btn btn-primary">Add New Product</Link>
                </div>
            </div>
        );
    }
}

export default Products;
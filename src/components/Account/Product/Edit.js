import React, { Component } from 'react';
import FormErrorsSignUp from '../../Login/FormErrorsSignUp';
import axios from 'axios';

class EditProduct extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id_product: '',
            id_user: '',
            accessToken: '',
            name: '',
            price: '',
            id_category: 1,
            catData: '',
            id_brand: 1,
            brandData: '',
            status: 0,
            sale: 0,
            company_profile: '',
            file: '',
            avatarCheckbox: [],
            avatarCheckboxList: [],
            dataProduct: [],
            detail: '',
            formErrors: {},
            fileType: ['png', 'jpg', 'jpeg']
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleUpdateProduct = this.handleUpdateProduct.bind(this)
        this.showCatInput = this.showCatInput.bind(this)
        this.showBrandInput = this.showBrandInput.bind(this)
        this.showSaleInput = this.showSaleInput.bind(this)
        this.showAvatarCheckbox = this.showAvatarCheckbox.bind(this)
        this.addToAvatarCkb = this.addToAvatarCkb.bind(this)
    }

    componentDidMount() {
        // console.log('componentDidMount')
        let id_product = this.props.match.params.id

        let userDataJson = localStorage.getItem('userData')
        if (userDataJson) {
            let userData = JSON.parse(userDataJson)
            let accessToken = userData.token
            axios({
                method: 'GET',
                url: 'http://localhost/laravel/laravel/public/api/user/product/' + id_product,
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }).then(res => {
                // console.log(res)
                console.log('dataProduct', res.data.data)
                var dataProduct = res.data.data

                this.setState({
                    id_product: id_product,
                    id_user: userData.id_user,
                    accessToken: accessToken,
                    dataProduct: dataProduct,
                    avatarCheckboxList: dataProduct.image,
                    name: dataProduct.name,
                    price: dataProduct.price,
                    id_category: dataProduct.id_category,
                    id_brand: dataProduct.id_brand,
                    status: dataProduct.status,
                    sale: dataProduct.sale,
                    company_profile: dataProduct.company_profile,
                    detail: dataProduct.detail,
                })
            })

            axios({
                method: 'GET',
                url: 'http://localhost/laravel/laravel/public/api/category-brand'
            }).then(res => {
                this.setState({
                    catData: res.data.category,
                    brandData: res.data.brand
                })
            })
        }
    }

    handleChange(e) {
        let target = e.target;
        let name = target.name;
        let value = target.value;
        let file = target.files

        this.setState({
            [name]: target.type == 'file' ? file : value
        })
    }

    handleUpdateProduct = (e) => {
        e.preventDefault()

        let allValid = true;
        let { id_product, id_user, name, price, id_category, id_brand, status, sale, company_profile, file, detail, avatarCheckbox } = this.state;

        let errorSubmit = this.state.formErrors;
        let avatarRemaining = 3 - avatarCheckbox.length
        // console.log(avatarRemaining)

        if (!name) {
            allValid = false;
            errorSubmit.name = 'Vui long nhap ten'
        } else {
            delete errorSubmit.name
        }

        if (!price) {
            allValid = false;
            errorSubmit.price = 'Vui long nhap gia'
        } else {
            delete errorSubmit.price
        }

        if (!id_category) {
            allValid = false;
            errorSubmit.category = 'Vui long nhap loai'
        } else {
            delete errorSubmit.category
        }

        if (!id_brand) {
            allValid = false;
            errorSubmit.brand = 'Vui long nhap thuong hieu'
        } else {
            delete errorSubmit.brand
        }

        if (!company_profile) {
            allValid = false;
            errorSubmit.company_profile = 'Vui long nhap company_profile'
        } else {
            delete errorSubmit.company_profile
        }
        
        if (!detail) {
            allValid = false;
            errorSubmit.detail = 'Vui long nhap detail'
        } else {
            delete errorSubmit.detail
        }

        console.log(file)
        if (file.length) {
            delete errorSubmit.avatarProduct
            if ((file.length + avatarRemaining) > 3) {
                allValid = false;
                errorSubmit.imgProduct = 'Tối đa 3 ảnh trên 1 sp. Vui lòng xóa bớt ảnh'
            } else {
                delete errorSubmit.imgProduct
                Object.values(file).map((fileItem, i) => {
                    console.log(fileItem)
                    if (fileItem.name) {
                        var fileFormat = fileItem.name.split('.')[1]
                        var fileSize = fileItem.size
                    }

                    let isPhoto = this.state.fileType.some((value, index) => {
                        return value === fileFormat;
                    })
                    // console.log(isPhoto)
                    // console.log(fileSize)
                    if (!(isPhoto && (fileSize < 1024 * 1024))) {
                        allValid = false;
                        errorSubmit.fileError = 'Chon file anh va co kich thuoc nho hon 1MB'
                        // console.log('Chon file anh va co kich thuoc nho lon 1MB')
                    } else {
                        delete errorSubmit.fileError
                    }
                })
            }
        } else {
            allValid = false;
            errorSubmit.avatarProduct = 'Vui lòng chọn ảnh'
        }

        if (!allValid) {
            this.setState({
                formErrors: errorSubmit
            })
        } else {
            this.setState({
                formErrors: {}
            })
            console.log(name, price, id_category, id_brand, status, sale, company_profile, file, detail, avatarCheckbox)
            const formData = new FormData();
            formData.append('id_user', id_user);
            formData.append('name', name);
            formData.append('price', price);
            formData.append('category', id_category);
            formData.append('brand', id_brand);
            formData.append('status', status);
            formData.append('sale', sale);
            formData.append('company', company_profile);
            formData.append('detail', detail);
            Object.keys(file).map((item, i) => {
                formData.append('file[]', file[item]);
                console.log(item)
            })
            avatarCheckbox.map((item, i) => {
                formData.append('avatarCheckbox[]', item);
                console.log(item)
            })
            axios({
                method: 'POST',
                url: 'http://localhost/laravel/laravel/public/api/user/edit-product/' + id_product,
                data: formData,
                headers: {
                    'Authorization': 'Bearer ' + this.state.accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }).then(res => {
                console.log(res)
                // alert('Thêm sản phẩm thành công!')
            }).catch(err => {
                console.log(err)
            })
        }
    }

    showCatInput() {
        let { catData } = this.state;

        if (catData) {
            return catData.map((value, index) => {
                return (
                    <option key={value.id} value={value.id}>{value.category}</option>
                )
            })
        }
    }

    showBrandInput() {
        let { brandData } = this.state;

        if (brandData) {
            return brandData.map((value, index) => {
                return (
                    <option key={value.id} value={value.id}>{value.brand}</option>
                )
            })
        }
    }

    showSaleInput(status, sale) {
        if (status == 0) {
            return (
                <div className="form-group">
                    <input type="text" value={sale} onChange={this.handleChange} name="sale" id="sale-input" className="form-control" placeholder="0" />
                    <span>%</span>
                </div>
            )
        } else {
            return null
        }
    }

    showAvatarCheckbox() {
        let { avatarCheckboxList, id_user } = this.state
        if (avatarCheckboxList) {
            // console.log(avatarCheckboxList)
            return avatarCheckboxList.map((imgProduct, i) => {
                return (
                    <div key={i} className="form-group img-checkbox-group">
                        <img
                            className='acc-product-img'
                            src={"http://localhost/laravel/laravel/public/upload/user/product/" + id_user + "/" + imgProduct} alt=""
                        >
                        </img>
                        <input type="checkbox" name='' onClick={() => { this.addToAvatarCkb(imgProduct) }} id="" className="img-checkbox" />
                    </div>
                )
            })

        }
    }

    addToAvatarCkb(imgProduct) {
        let { avatarCheckbox } = this.state

        if (avatarCheckbox) {
            let avatar = '';
            avatar = avatarCheckbox.find((value, i) => {
                return imgProduct == value;
            })
            if (!avatar) {
                avatarCheckbox.push(imgProduct)
                this.setState({
                    avatarCheckbox: avatarCheckbox
                })
            } else {
                avatarCheckbox.splice(avatarCheckbox.indexOf(avatar), 1)
                this.setState({
                    avatarCheckbox: avatarCheckbox
                })
            }
        } else {
            avatarCheckbox.push(imgProduct)
            this.setState({
                avatarCheckbox: avatarCheckbox
            })
        }
    }

    render() {
        // console.log('render')
        let { name, price, company_profile, detail, status, sale } = this.state;

        return (
            <div className="col-sm-8">
                <div id='add-product'>
                    <h2 className='my-product-label'>Edit Product</h2>
                    <form onSubmit={this.handleUpdateProduct}>
                        <div className="form-group">
                            <input type="text" value={name} onChange={this.handleChange} name="name" id="" className="form-control" placeholder="Product Name" />
                        </div>
                        <div className="form-group">
                            <input type="text" value={price} onChange={this.handleChange} name="price" id="" className="form-control" placeholder="Product Price" />
                        </div>
                        <div className="form-group">
                            <select className="form-control" name="id_category" value = {this.state.id_category} id="" onChange={this.handleChange}>
                                {this.showCatInput()}
                            </select>
                        </div>
                        <div className="form-group">
                            <select className="form-control" name="id_brand" value = {this.state.id_brand} id="" onChange={this.handleChange}>
                                {this.showBrandInput()}
                            </select>
                        </div>
                        <div className="form-group">
                            <select className="form-control" name="status" value = {this.state.status} id="" onChange={this.handleChange}>
                                <option value={0}>sale</option>
                                <option value={1}>new</option>
                            </select>
                        </div>
                        {this.showSaleInput(status, sale)}
                        <div className="form-group">
                            <input type="text" value={company_profile} onChange={this.handleChange} name="company_profile" id="" className="form-control" placeholder="Company Profile" />
                        </div>
                        <div className="form-group">
                            <input type="file" onChange={this.handleChange} name="file" multiple id="" className="form-control" placeholder="" />
                        </div>
                        {this.showAvatarCheckbox()}
                        <div className="form-group">
                            <textarea value={detail} onChange={this.handleChange} className="form-control" name="detail" placeholder="Detail" id="" rows="1"></textarea>
                        </div>
                        <div className='add-product-btn'>
                            <button type="submit" className="btn btn-primary add-product-btn">Update</button>
                        </div>
                    </form>
                </div>
                <FormErrorsSignUp formErrorsSignUp={this.state.formErrors} />
            </div>
        );
    }
}

export default EditProduct;
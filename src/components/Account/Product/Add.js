import React, { Component } from 'react';
import FormErrorsSignUp from '../../Login/FormErrorsSignUp';
import axios from 'axios';

class Add extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id_user: '',
            accessToken: '',
            name: '',
            price: '',
            category: 1,
            catData: '',
            brand: 1,
            brandData: '',
            status: 0,
            sale: 0,
            company: '',
            file: '',
            detail: '',
            formErrors: {},
            fileType: ['png', 'jpg', 'jpeg']
        }

        this.handleChange = this.handleChange.bind(this)
    }

    componentDidMount() {
        let userDataJson = localStorage.getItem('userData')
        if (userDataJson) {
            let userData = JSON.parse(userDataJson)
            let accessToken = userData.token
            this.setState({
                id_user: userData.id_user,
                accessToken: accessToken
            })
        }

        axios({
            method: 'GET',
            url: 'http://localhost/laravel/laravel/public/api/category-brand'
        }).then(res => {
            console.log(res)
            this.setState({
                catData: res.data.category,
                brandData: res.data.brand
            })
        })
    }

    handleChange(e) {
        let target = e.target;
        let name = target.name;
        let value = target.value;
        let file = target.files

        this.setState({
            [name]: target.type == 'file' ? file : value
        })
    }

    handleAddProduct = (e) => {
        e.preventDefault()

        let allValid = true;
        let { id_user, name, price, category, brand, status, sale, company, file, detail } = this.state;

        let errorSubmit = this.state.formErrors;

        if (!name) {
            allValid = false;
            errorSubmit.name = 'Vui long nhap ten'
        } else {
            delete errorSubmit.name
        }

        if (!price) {
            allValid = false;
            errorSubmit.price = 'Vui long nhap gia'
        } else {
            delete errorSubmit.price
        }

        if (!category) {
            allValid = false;
            errorSubmit.category = 'Vui long nhap loai'
        } else {
            delete errorSubmit.category
        }

        if (!brand) {
            allValid = false;
            errorSubmit.brand = 'Vui long nhap thuong hieu'
        } else {
            delete errorSubmit.brand
        }

        if (!company) {
            allValid = false;
            errorSubmit.company = 'Vui long nhap company_profile'
        } else {
            delete errorSubmit.company
        }
        
        if (!detail) {
            allValid = false;
            errorSubmit.detail = 'Vui long nhap detail'
        } else {
            delete errorSubmit.detail
        }

        console.log(file)
        // console.log()
        if (file.length > 3 || file === '') {
            allValid = false;
            errorSubmit.imgProduct = 'Vui long chon hinh va toi da 3 hinh'
        } else {
            delete errorSubmit.imgProduct
            Object.values(file).map((fileItem, i) => {
                console.log(fileItem)
                if (fileItem.name) {
                    var fileFormat = fileItem.name.split('.')[1]
                    var fileSize = fileItem.size
                }

                let isPhoto = this.state.fileType.some((value, index) => {
                    return value === fileFormat;
                })
                // console.log(isPhoto)
                // console.log(fileSize)
                if (!(isPhoto && (fileSize < 1024 * 1024))) {
                    allValid = false;
                    errorSubmit.fileError = 'Chon file anh va co kich thuoc nho hon 1MB'
                    // console.log('Chon file anh va co kich thuoc nho lon 1MB')
                } else {
                    delete errorSubmit.file
                }
            })
        }

        if (!allValid) {
            this.setState({
                formErrors: errorSubmit
            })
        } else {
            this.setState({
                formErrors: {}
            })
            console.log(name, price, category, brand, status, sale, company, file, detail)
            const formData = new FormData();
            formData.append('id_user', id_user);
            formData.append('name', name);
            formData.append('price', price);
            formData.append('category', category);
            formData.append('brand', brand);
            formData.append('status', status);
            formData.append('sale', sale);
            formData.append('company', company);
            formData.append('detail', detail);
            Object.keys(file).map((item, i) => {
                formData.append('file[]', file[item]);
                console.log(item)
            })
            axios({
                method: 'POST',
                url: 'http://localhost/laravel/laravel/public/api/user/add-product',
                data: formData,
                headers: {
                    'Authorization': 'Bearer ' + this.state.accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }).then(res => {
                console.log(res)
                // alert('Thêm sản phẩm thành công!')
            }).catch(err => {
                console.log(err)
            })
        }
    }

    showCatInput() {
        let { catData } = this.state;

        if (catData) {
            return catData.map((value, index) => {
                return (
                    <option key={value.id} value={value.id}>{value.category}</option>
                )
            })
        }
    }

    showBrandInput() {
        let { brandData } = this.state;

        if (brandData) {
            return brandData.map((value, index) => {
                return (
                    <option key={value.id} value={value.id}>{value.brand}</option>
                )
            })
        }
    }

    showSaleInput(status) {
        if (status == '0') {
            return (
                <div className="form-group">
                    <input type="text" onChange={this.handleChange} name="sale" id="sale-input" className="form-control" placeholder="0" />
                    <span>%</span>
                </div>
            )
        } else {
            return null
        }
    }

    render() {
        let { status } = this.state;

        return (
            <div className="col-sm-8">
                <div id='add-product'>
                    <h2 className='my-product-label'>Add A New Product</h2>
                    <form onSubmit={this.handleAddProduct}>
                        <div className="form-group">
                            <input type="text" onChange={this.handleChange} name="name" id="" className="form-control" placeholder="Product Name" />
                        </div>
                        <div className="form-group">
                            <input type="text" onChange={this.handleChange} name="price" id="" className="form-control" placeholder="Product Price" />
                        </div>
                        <div className="form-group">
                            <select className="form-control" name="category" id="" onChange={this.handleChange}>
                                {this.showCatInput()}
                            </select>
                        </div>
                        <div className="form-group">
                            <select className="form-control" name="brand" id="" onChange={this.handleChange}>
                                {this.showBrandInput()}
                            </select>
                        </div>
                        <div className="form-group">
                            <select className="form-control" name="status" id="" onChange={this.handleChange}>
                                <option value={0}>sale</option>
                                <option value={1}>new</option>
                            </select>
                        </div>
                        {this.showSaleInput(status)}
                        <div className="form-group">
                            <input type="text" onChange={this.handleChange} name="company" id="" className="form-control" placeholder="Company Profile" />
                        </div>
                        <div className="form-group">
                            <input type="file" onChange={this.handleChange} name="file" multiple id="" className="form-control" placeholder="" />
                        </div>
                        <div className="form-group">
                            <textarea onChange={this.handleChange} className="form-control" name="detail" placeholder="Detail" id="" rows="1"></textarea>
                        </div>
                        <div className='add-product-btn'>
                            <button type="submit" className="btn btn-primary add-product-btn">Add</button>
                        </div>
                    </form>
                </div>
                <FormErrorsSignUp formErrorsSignUp={this.state.formErrors} />
            </div>
        );
    }
}

export default Add;
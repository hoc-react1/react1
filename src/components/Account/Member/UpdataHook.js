import React, { useEffect, useState } from "react";
import FormErrorsSignUpHook from "../../Login/FormErrorsSignUpHook";
import axios from "axios";

function UpdateHook() {
    const [accessToken, setAccessToken] = useState('')
    const [idUser, setIdUser] = useState('')

    const [inputs, setInputs] = useState('')

    const [avatar, setAvatar] = useState('')
    const [formErrors, setFormErrors] = useState({})
    const [fileType, setFileType] = useState(['png', 'jpg', 'jpeg'])
    const [isUpdateSucc, setIsUpdateSucc] = useState('')

    useEffect(() => {
        const userDatajson = localStorage.getItem('userData')

        if (userDatajson) {
            let userData = JSON.parse(userDatajson)
            let accessToken = userData.token
            console.log(userData)

            setAccessToken(accessToken)
            setIdUser(userData.id_user)

            setInputs({
                username: userData.name_user,
                password: userData.password_user,
                email: userData.email_user,
                phone: userData.phone_user,
                address: userData.address_user
            })
        }
    }, [])

    const handleInput = (e) => {
        let nameInput = e.target.name;
        let value = e.target.value;
        if (e.target.type == 'file') {
            value = e.target.files[0]
        }

        setInputs(state => ({
            ...state, [nameInput]: value
        }))

        if (e.target.files) {
            let reader = new FileReader();

            reader.onload = (e) => [
                setAvatar(e.target.result)
            ]
            reader.readAsDataURL(e.target.files[0])
        }
    }

    const handleSubmitUpdate = (e) => {
        e.preventDefault()

        let allValid = true;
        let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        let errorSubmit = formErrors;

        if (!inputs.username) {
            allValid = false;
            errorSubmit.username = 'Vui long nhap ten'
        } else {
            delete errorSubmit.username
        }

        if (!(regex.test(inputs.email))) {
            allValid = false;
            errorSubmit.email = 'Vui long nhap email'
        } else {
            delete errorSubmit.email
        }

        if (!inputs.password) {
            allValid = false;
            errorSubmit.password = 'Vui long nhap mat khau'
        } else {
            delete errorSubmit.password
        }

        // console.log(typeof phone)
        // console.log(phone !== '')
        // console.log(phone.length >= 10)

        // console.log(inputs.phone)
        if (inputs.phone) {
            if (!(inputs.phone !== '' && inputs.phone.length >= 10)) {
                allValid = false;
                errorSubmit.phone = 'Vui long nhap so dien thoai'
            } else {
                delete errorSubmit.phone
            }
        } else {
            allValid = false;
            errorSubmit.phone = 'Vui long nhap so dien thoai'
        }

        if (!inputs.address) {
            allValid = false;
            errorSubmit.address = 'Vui long nhap dia chi'
        } else {
            delete errorSubmit.address
        }

        // console.log(inputs.file)
        if (inputs.file) {
            if (inputs.file.name) {
                var fileFormat = inputs.file.name.split('.')[1]
                var fileSize = inputs.file.size
            }

            let isPhoto = fileType.some((value, index) => {
                return value === fileFormat;
            })

            if (!(isPhoto && (fileSize < 1024 * 1024))) {
                allValid = false;
                errorSubmit.fileError = 'Chon file anh va co kich thuoc nho hon 1MB'
            } else {
                console.log('delete')
                delete errorSubmit.fileError
            }
        } else {
            allValid = false
            errorSubmit.fileError = 'Chon file anh va co kich thuoc nho hon 1MB'
        }

        if (!allValid) {
            setFormErrors({ ...errorSubmit })
        } else {
            setFormErrors({})
            const formData = new FormData();
            formData.append('name', inputs.username);
            formData.append('email', inputs.email);
            formData.append('password', inputs.password);
            formData.append('address', inputs.address);
            formData.append('phone', inputs.phone);
            formData.append('avatar', avatar);
            axios({
                method: 'POST',
                url: 'http://localhost/laravel/laravel/public/api/user/update/' + idUser,
                data: formData,
                headers: {
                    'Authorization': 'Bearer ' + accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }).then(res => {
                console.log(res)
                console.log(res.data)
                if (res.data.response === 'success') {
                    let userData = {}
                    userData['token'] = res.data.success.token
                    userData['id_user'] = res.data.Auth.id
                    userData['name_user'] = res.data.Auth.name
                    userData['image_user'] = res.data.Auth.avatar
                    userData['email_user'] = res.data.Auth.email
                    userData['phone_user'] = res.data.Auth.phone
                    userData['address_user'] = res.data.Auth.address
                    userData['password_user'] = inputs.password
                    let userDataJson = JSON.stringify(userData)
                    localStorage.setItem('userData', userDataJson)
                }

                // this.setState({
                //     accessToken: res.data.success.token,
                //     id_user: res.data.Auth.id,
                //     name: res.data.Auth.name,
                //     avatar: res.data.Auth.avatar,
                //     password: this.state.password,
                //     email: res.data.Auth.email,
                //     phone: res.data.Auth.phone,
                //     address: res.data.Auth.address
                // })

                setIsUpdateSucc('Cập nhật tài khoản thành công!')
            }).catch(err => {
                console.log(err)
            })
        }
    }

    return (
        <section id="form-update">{/*form*/}
            <div className="container">
                <div className="row">
                    <div className="col-sm-6">
                        <div className="signup-form">{/*sign up form*/}
                            <h2>Update Account Information</h2>
                            <form onSubmit={handleSubmitUpdate} encType='multipart/form-data'>
                                <input type="text" name="username" value={inputs.username} onChange={handleInput} placeholder="Name" />
                                <input type="text" name="email" value={inputs.email} onChange={handleInput} placeholder="Email Address" />
                                <input type="password" name="password" value={inputs.password} onChange={handleInput} placeholder="Password" />
                                <input type="number" name="phone" value={inputs.phone} onChange={handleInput} placeholder="Phone" />
                                <input type="text" name="address" value={inputs.address} onChange={handleInput} placeholder="Address" />
                                <input type="file" name="file" onChange={handleInput} />
                                <button type="submit" className="btn btn-default">Update Account</button>
                            </form>
                            <p>{isUpdateSucc}</p>
                        </div>{/*/sign up form*/}<br />
                        <FormErrorsSignUpHook formErrorsSignUp={formErrors} />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default UpdateHook;
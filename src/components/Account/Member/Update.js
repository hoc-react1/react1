import React, { Component } from "react";
import FormErrorsSignUp from "../../Login/FormErrorsSignUp";
import axios from "axios";

class Update extends Component {
    constructor(props) {
        super(props);

        this.state = {
            accessToken: '',
            id_user: '',
            name: '',
            password: '',
            email: '',
            phone: '',
            address: '',
            file: '',
            avatar: '',
            formErrors: {},
            fileType: ['png', 'jpg', 'jpeg']
        }

        this.handleInput = this.handleInput.bind(this)
        this.handleSubmitUpdate = this.handleSubmitUpdate.bind(this)
    }

    componentDidMount() {
        const userDatajson = localStorage.getItem('userData')

        if (userDatajson) {
            let userData = JSON.parse(userDatajson)
            let accessToken = userData.token
            // console.log(userData)
            this.setState({
                accessToken: accessToken,
                id_user: userData.id_user,
                name: userData.name_user,
                password: userData.password_user,
                email: userData.email_user,
                phone: userData.phone_user,
                address: userData.address_user
            })
        }
    }

    handleInput = (e) => {
        let target = e.target;
        let nameInput = target.name;
        let value = target.value;
        const file = e.target.files;

        this.setState({
            [nameInput]: (target.type === 'file' ? target.files[0] : value)
        })

        if (file) {
            let reader = new FileReader();

            reader.onload = (e) => [
                this.setState({
                    avatar: e.target.result
                })
            ]
            reader.readAsDataURL(file[0])
        }
        // console.log(target.files)
    }

    handleSubmitUpdate = (e) => {
        e.preventDefault()

        let allValid = true;
        let { id_user, name, email, password, phone, address, file, avatar } = this.state;
        let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        let errorSubmit = this.state.formErrors;

        if (!name) {
            allValid = false;
            errorSubmit.name = 'Vui long nhap ten'
        } else {
            delete errorSubmit.name
        }

        if (!(regex.test(email))) {
            allValid = false;
            errorSubmit.email = 'Vui long nhap email'
        } else {
            delete errorSubmit.email
        }

        if (!password) {
            allValid = false;
            errorSubmit.password = 'Vui long nhap mat khau'
        } else {
            delete errorSubmit.password
        }

        console.log(typeof phone)
        console.log(phone !== '')
        console.log(phone.length >= 10)
        if (!(phone !== '' && phone.length >= 10)) {
            allValid = false;
            errorSubmit.phone = 'Vui long nhap so dien thoai'
            console.log('123')
        } else {
            delete errorSubmit.phone
        }

        if (!address) {
            allValid = false;
            errorSubmit.address = 'Vui long nhap dia chi'
        } else {
            delete errorSubmit.address
        }

        console.log(file)
        if (file) {
            var fileFormat = file.name.split('.')[1]
            var fileSize = file.size

            let isPhoto = this.state.fileType.some((value, index) => {
                return value === fileFormat;
            })
            // console.log(isPhoto)
            // console.log(fileSize)
            if (!(isPhoto && (fileSize < 1024 * 1024))) {
                allValid = false;
                errorSubmit.fileError = 'Chon file anh va co kich thuoc nho hon 1MB'
            } else {
                delete errorSubmit.file
            }
        }

        if (!allValid) {
            this.setState({
                formErrors: errorSubmit
            })
        } else {
            this.setState({
                formErrors: {}
            })
            console.log(name, address, email, phone)
            const formData = new FormData();
            formData.append('name', name);
            formData.append('email', email);
            formData.append('password', password);
            formData.append('address', address);
            formData.append('phone', phone);
            formData.append('avatar', avatar);
            axios({
                method: 'POST',
                url: 'http://localhost/laravel/laravel/public/api/user/update/' + id_user,
                data: formData,
                headers: {
                    'Authorization': 'Bearer ' + this.state.accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                }
            }).then(res => {
                console.log(res)
                console.log(res.data)
                if (res.data.response === 'success') {
                    let userData = {}
                    userData['token'] = res.data.success.token
                    userData['id_user'] = res.data.Auth.id
                    userData['name_user'] = res.data.Auth.name
                    userData['image_user'] = res.data.Auth.avatar
                    userData['email_user'] = res.data.Auth.email
                    userData['phone_user'] = res.data.Auth.phone
                    userData['address_user'] = res.data.Auth.address
                    userData['password_user'] = this.state.password
                    let userDataJson = JSON.stringify(userData)
                    localStorage.setItem('userData', userDataJson)
                }

                this.setState({
                    accessToken: res.data.success.token,
                    id_user: res.data.Auth.id,
                    name: res.data.Auth.name,
                    avatar: res.data.Auth.avatar,
                    password: this.state.password,
                    email: res.data.Auth.email,
                    phone: res.data.Auth.phone,
                    address: res.data.Auth.address
                })

                alert('Cập nhật thông tin Thành Công!')
            }).catch(err => {
                console.log(err)
            })
        }
    }


    render() {
        return (
            <section id="form-update">{/*form*/}
                <div className="container">
                    <div className="row">
                        <div className="col-sm-6">
                            <div className="signup-form">{/*sign up form*/}
                                <h2>Update Account Information</h2>
                                <form onSubmit={this.handleSubmitUpdate} encType='multipart/form-data'>
                                    <input type="text" name="name" value={this.state.name} onChange={this.handleInput} placeholder="Name" />
                                    <input type="text" name="email" value={this.state.email} onChange={this.handleInput} placeholder="Email Address" readOnly />
                                    <input type="password" name="password" value={this.state.password} onChange={this.handleInput} placeholder="Password" />
                                    <input type="number" name="phone" value={this.state.phone} onChange={this.handleInput} placeholder="Phone" />
                                    <input type="text" name="address" value={this.state.address} onChange={this.handleInput} placeholder="Address" />
                                    <input type="file" name="file" onChange={this.handleInput} />
                                    <button type="submit" className="btn btn-default">Update Account</button>
                                </form>
                            </div>{/*/sign up form*/}<br />
                            <FormErrorsSignUp formErrorsSignUp={this.state.formErrors} />
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
export default Update;
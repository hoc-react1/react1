import React, { Component } from 'react';
import {
    Switch,
    Route
} from 'react-router-dom';
import App from './App';
import Update from './Member/Update';
import UpdateHook from './Member/UpdataHook';
import Products from './Product/Products';
import AddProduct from './Product/Add';
import EditProduct from './Product/Edit';

class Index extends Component {
    render() {
        return (
            <App>
                <Switch>
                    {/* <Route exact path = '/account' component = {Update} />
                    <Route exact path = '/account/member' component = {Update} /> */}
                    <Route exact path = '/account' component = {UpdateHook} />
                    <Route exact path = '/account/member' component = {UpdateHook} />
                    <Route path = '/account/product/list' component = {Products} />
                    <Route path = '/account/product/add' component = {AddProduct} />
                    <Route path = '/account/product/edit/:id' component = {EditProduct} />
                </Switch>
            </App>
        );
    }
}

export default Index;
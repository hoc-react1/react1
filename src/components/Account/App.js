import React, { Component } from 'react';
import MenuLeft from './MenuLeft';

class App extends Component {
    render() {
        return (
            <div>
                <div className='container'>
                    <div className='row'>
                        <MenuLeft />

                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
import React, { Component } from "react";

class ListComment extends Component {
    // constructor(props) {
    // 	super(props)
    // }

    showCommentItem(listComment) {
        console.log(listComment)
        if (listComment) {
            let commentItem = listComment.map((value, index) => {
                if (value.id_comment == 0) {
                    console.log('listComment', listComment)
                    console.log('value', value)
                    return (
                        <div key={index}>
                            <li key={index} className="media" >
                                <a className="pull-left" href>
                                    <img className="media-object" src="images/blog/man-two.jpg" alt="" />
                                </a>
                                <div className="media-body">
                                    <ul className="sinlge-post-meta">
                                        <li><i className="fa fa-user" />Janis Gallagher</li>
                                        <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                        <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                                    </ul>
                                    <p>{value.comment}</p>
                                    <a id={value.id} onClick={() => { this.onReply(value.id) }} className="btn btn-primary" href='#text-area'><i className="fa fa-reply" />Replay</a>
                                </div>
                            </li>
                            {
                                listComment.map((value2, index2) => {
                                    if (value.id == value2.id_comment) {
                                        return (
                                            <li key={index2} className="media second-media" >
                                                <a className="pull-left" href>
                                                    <img className="media-object" src="images/blog/man-three.jpg" alt="" />
                                                </a>
                                                <div className="media-body">
                                                    <ul className="sinlge-post-meta">
                                                        <li><i className="fa fa-user" />Janis Gallagher</li>
                                                        <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                                        <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                                                    </ul>
                                                    <p>{value2.comment}</p>
                                                    <a className="btn btn-primary" href><i className="fa fa-reply" />Replay</a>
                                                </div>
                                            </li>
                                        )
                                    }
                                })
                            }
                        </div>
                    )
                }
            })
            return (commentItem)
        }
    }

    onReply(idSubComment) {
        this.props.receiveIdCmt(idSubComment)
    }

    render() {
        let { listComment } = this.props

        return (
            <ul className="media-list" id='media-list-cmt'>
                {this.showCommentItem(listComment)}
            </ul>
        );
    }
}

export default ListComment;

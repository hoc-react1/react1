import React, { Component } from "react";
import axios from "axios";
import ListComment from './ListComment';
import PostComment from "./PostComment";
import Rate from "./Rate";

class DetailBlog extends Component {
    constructor(props) {
        super(props)

        this.state = {
            blogDetailData: [],
            cmtInput: '',
            commentData: [],
            idBlog: '',
            idSubComment: ''
        }

        this.getCmtData = this.getCmtData.bind(this)
        this.receiveIdCmt = this.receiveIdCmt.bind(this)
    }

    componentDidMount() {
        axios({
            method: 'GET',
            url: 'http://localhost/laravel/laravel/public/api/blog/detail/' + this.props.match.params.id,
            data: null
        }).then(res => {
            // console.log(res.data)
            this.setState({
                blogDetailData: res.data.data,
                commentData: res.data.data.comment,
                idBlog: this.props.match.params.id
            })
            localStorage.setItem('idBlog', JSON.stringify(this.props.match.params.id))
            // console.log('blogDetailData:', this.state.blogDetailData)
        }).catch(err => {
            console.log(err)
        })
    }

    getCmtData(cmtData) {
        let { commentData } = this.state
        commentData.splice(0, 0, cmtData)

        this.setState({
            commentData: commentData
        })
    }

    receiveIdCmt(idSubComment) {
        this.setState({
            idSubComment: idSubComment
        })
    }

    changeRating(newRating, name) {
        this.setState({
            rating: newRating
        });
    }

    render() {
        // console.log(this.state.blogDetailData.id)
        let { content, description, image, title } = this.state.blogDetailData

        return (
            <div className="col-sm-9">
                <div className="blog-post-area">
                    <h2 className="title text-center">Latest From our Blog</h2>
                    <div className="single-blog-post">
                        <h3>{title}</h3>
                        <div className="post-meta">
                            <ul>
                                <li><i className="fa fa-user" /> Mac Doe</li>
                                <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                            </ul>
                            {/* <span>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
								</span> */}
                        </div>
                        <a href>
                            <img src={"http://localhost/laravel/laravel/public/upload/Blog/image/" + image} alt="" />
                        </a>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p> <br />
                        <p>
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p> <br />
                        <p>
                            Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p> <br />
                        <p>
                            Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                        </p>
                        <div className="pager-area">
                            <ul className="pager pull-right">
                                <li><a href="#">Pre</a></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>{/*/blog-post-area*/}
                <Rate 
                    idBlog={this.state.idBlog}
                />{/*/rating-area*/}
                <div className="socials-share">
                    <a href><img src="images/blog/socials.png" alt="" /></a>
                </div>{/*/socials-share*/}
                {/* <div class="media commnets">
						<a class="pull-left" href="#">
							<img class="media-object" src="images/blog/man-one.jpg" alt="">
						</a>
						<div class="media-body">
							<h4 class="media-heading">Annie Davis</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<div class="blog-socials">
								<ul>
									<li><a href=""><i class="fa fa-facebook"></i></a></li>
									<li><a href=""><i class="fa fa-twitter"></i></a></li>
									<li><a href=""><i class="fa fa-dribbble"></i></a></li>
									<li><a href=""><i class="fa fa-google-plus"></i></a></li>
								</ul>
								<a class="btn btn-primary" href="">Other Posts</a>
							</div>
						</div>
					</div> */}{/*Comments*/}
                <div className="response-area">
                    <h2>3 RESPONSES</h2>
                    <ListComment
                        listComment={this.state.commentData}
                        receiveIdCmt={this.receiveIdCmt}
                    />
                </div>{/*/Response-area*/}
                <PostComment
                    receiveCmt={this.getCmtData}
                    idBlog={this.state.idBlog}
                    idSubComment={this.state.idSubComment}
                    history={this.props.history}
                />
            </div>
        )
    }
}

export default DetailBlog;
import React, { Component } from 'react';
import axios from 'axios';
import StarRatings from 'react-star-ratings';

class Rate extends Component {
    constructor(props) {
        super(props)
        this.state = {
            rating: 0,
            rateData: [],
            idBlog: ''
        }
        this.changeRating = this.changeRating.bind(this)
    }

    componentDidMount() {
        let idBlog = JSON.parse(localStorage.getItem('idBlog'))
        console.log('idBlog', idBlog)
        axios({
            method: 'GET',
            url: 'http://localhost/laravel/laravel/public/api/blog/rate/' + idBlog
        }).then(res => {
            let rateData = res.data.data
            console.log(rateData)
            let ratePoint = 0;
            rateData.map((value, index) => {
                return ratePoint += value.rate;
            });
            console.log(ratePoint)
            this.setState({
                rating: Math.round(ratePoint/(rateData.length)),
                rateData: rateData
            })
        }).catch(err => {
            console.log(err)
        })
    }

    changeRating(newRating, name) {
        // console.log(newRating)
        this.setState({
            rating: newRating
        });

        const userDatajson = localStorage.getItem('userData')

        if (userDatajson) {
            let userData = JSON.parse(userDatajson)
            console.log(userData)
            let accessToken = userData.token

            if (newRating) {
                const formData = new FormData();
                formData.append('blog_id', this.props.idBlog);
                formData.append('user_id', userData.id_user);
                formData.append('rate', newRating);
                axios({
                    method: 'POST',
                    url: 'http://localhost/laravel/laravel/public/api/blog/rate/' + this.props.idBlog,
                    data: formData,
                    headers: {
                        'Authorization': 'Bearer ' + accessToken,
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': 'application/json'
                    }
                }).then(res => {
                    // console.log(res)
                })
            }
        }
    }

    render() {
        // console.log(this.props.idBlog)
        return (
            <div className="rating-area">
                <ul className="ratings">
                    <li className="rate-this">Rate this item:</li>
                    <StarRatings
                        rating={this.state.rating}
                        starRatedColor="yellow"
                        changeRating={this.changeRating}
                        numberOfStars={5}
                        name='rating'
                    />
                    <li className="color">({this.state.rateData.length} votes)</li>
                </ul>
                <ul className="tag">
                    <li>TAG:</li>
                    <li><a className="color" href>Pink <span>/</span></a></li>
                    <li><a className="color" href>T-Shirt <span>/</span></a></li>
                    <li><a className="color" href>Girls</a></li>
                </ul>
            </div>
        );
    }
}

export default Rate;
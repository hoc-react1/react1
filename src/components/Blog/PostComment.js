import React, { Component } from "react";
import axios from "axios";

class PostComment extends Component {
    constructor(props) {
        super(props)

        this.state = {
            cmtInput: '',
        }

        this.postComment = this.postComment.bind(this)
        this.handleCmtBox = this.handleCmtBox.bind(this)
    }

    handleCmtBox(e) {
        let target = e.target;
        let nameInput = target.name;
        let value = target.value;

        this.setState({
            [nameInput]: value
        })
    }

    postComment() {
        let isLoginJson = localStorage.getItem('isLogin')
        if (isLoginJson) {
            var isLogin = JSON.parse(isLoginJson)
        }
        
        // console.log(isLogin)
        if (isLogin) {
            if (this.state.cmtInput === '') {
                document.getElementById('cmtWarning').innerHTML = 'Vui lòng nhập bình luận'
            } else {
                document.getElementById('cmtWarning').innerHTML = ''
                const userDatajson = localStorage.getItem('userData')
                // console.log(userDatajson)
                if (userDatajson) {
                    let userData = JSON.parse(userDatajson)
                    // console.log(userData)
                    let accessToken = userData.token

                    let { cmtInput } = this.state
                    if (cmtInput) {
                        const formData = new FormData();
                        formData.append('id_blog', this.props.idBlog);
                        formData.append('id_user', userData.id_user);
                        formData.append('id_comment', this.props.idSubComment ? this.props.idSubComment : 0);
                        formData.append('comment', this.state.cmtInput);
                        formData.append('image_user', userData.image_user);
                        formData.append('name_user', userData.name_user);
                        axios({
                            method: 'POST',
                            url: 'http://localhost/laravel/laravel/public/api/blog/comment/' + this.props.idBlog,
                            data: formData,
                            headers: {
                                'Authorization': 'Bearer ' + accessToken,
                                'Content-Type': 'application/x-www-form-urlencoded',
                                'Accept': 'application/json'
                            }
                        }).then(res => {
                            // console.log(res)
                            console.log(res.data.data)

                            this.getCmtData(res.data.data)

                            this.setState({
                                cmtInput: ''
                            })

                        })
                    }
                }
            }
        } else {
            //eslint-disable-next-line no-restricted-globals
            let confirmPost = confirm('Vui lòng đăng nhập trước khi comment!')
            if (confirmPost) {
                this.props.history.push('/login')
            }
        }
    }

    getCmtData(cmtData) {
        this.props.receiveCmt(cmtData)
    }

    render() {
        return (
            <div className="replay-box">
                <div className="row">
                    <div className="col-sm-12">
                        <h2>Leave a replay</h2>
                        <div className="text-area">
                            <div className="blank-arrow">
                                <label>Your Name</label>
                            </div>
                            <span>*</span>
                            <textarea id = 'text-area' name="cmtInput" value={this.state.cmtInput} onChange={this.handleCmtBox} rows={11} />
                            <p id='cmtWarning'></p>
                            <a href className="btn btn-primary" onClick={this.postComment}>Post comment</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PostComment;

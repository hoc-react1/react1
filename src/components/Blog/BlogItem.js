import React, { Component } from "react";
import { Link } from "react-router-dom";


class BlogItem extends Component {
    render() {
        let { id, title, img, desc, content } = this.props;
        console.log(content)
        return (
            <div className="single-blog-post">
                <h3>{title}</h3>
                <div className="post-meta">
                    <ul>
                        <li><i className="fa fa-user" /> Mac Doe</li>
                        <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                        <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                    </ul>
                    <span>
                        <i className="fa fa-star" />
                        <i className="fa fa-star" />
                        <i className="fa fa-star" />
                        <i className="fa fa-star" />
                        <i className="fa fa-star-half-o" />
                    </span>
                </div>
                <Link to={'/blog/list/detail/' + id}>
                    <img src={"http://localhost/laravel/laravel/public/upload/Blog/image/" + img} alt="" />
                </Link>
                <div>{content}</div>
                <Link to={'/blog/list/detail/' + id} className="btn btn-primary" href>Read More</Link>
            </div>
        )
    }
}

export default BlogItem
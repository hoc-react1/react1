import axios from "axios";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import BlogItem from './BlogItem'

class BlogList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            blogData : []
        }
    }

    componentDidMount() {
        axios({
            method : 'GET',
            url: 'http://localhost/laravel/laravel/public/api/blog',
            data: null
        }).then(res => {
            // console.log(res.data)
            this.setState({
                blogData : res.data.blog.data
            })
            console.log('blogData:', res.data.blog.data)
        }).catch(err => {
            console.log(err)
        })
    }

    showBlogList() {
        let {blogData} = this.state
        
        return blogData.map((value, index) => {
            return (
                <BlogItem 
                    key = {index}
                    id = {value.id}
                    title = {value.title}
                    img = {value.image}
                    desc = {value.description}
                    content = {value.content}
                />
            )
        })
        
    }

    render() {
        return (
            <div className="col-sm-9">
                <div className="blog-post-area">
                    <h2 className="title text-center">Latest From our Blog</h2>
                    {this.showBlogList()}
                    <div className="pagination-area">
                        <ul className="pagination">
                            <li><a href className="active">1</a></li>
                            <li><a href>2</a></li>
                            <li><a href>3</a></li>
                            <li><a href><i className="fa fa-angle-double-right" /></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default BlogList
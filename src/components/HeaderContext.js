import { useState, createContext, useEffect } from "react";

const Context = createContext()

function HeaderProvider({children}) {
    const [cartQty, setCartQty] = useState(10)
    const [isLogin, setIsLogin] = useState(false)

    const value = {
        cartQty,
        setCartQty,
        isLogin,
        setIsLogin
    }

    useEffect(() => {
        let cartQtyJson = localStorage.getItem('cartQty')
        let isLoginJson = localStorage.getItem('isLogin')
        if (isLoginJson) {
            setIsLogin(JSON.parse(isLoginJson))
        }
        if (cartQtyJson) {
            setCartQty(JSON.parse(cartQtyJson))
        }
    }, [])

    return(
        <Context.Provider value = {value}>
            {children}
        </Context.Provider>
    )
}

export {Context, HeaderProvider}
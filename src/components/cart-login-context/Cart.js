import React, { Component } from 'react';
import axios from 'axios';
import { MyContext } from '../MyProvider';

class Cart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            productQtyAll: 0,
            cartQty: 0,
            productCart: '',
            cartTotal: 0
        }
    }

    componentDidMount() {
        let cartTotal = 0
        let productCartJon = localStorage.getItem('productCart')
        let cartQtyJson = localStorage.getItem('cartQty')
        let productQtyAllJson = localStorage.getItem('productQtyAll')
        if (productQtyAllJson) {
            var productQtyAll = JSON.parse(productQtyAllJson)
        }
        if (cartQtyJson) {
            var cartQty = JSON.parse(cartQtyJson)
        }
        if (productCartJon) {
            var productCart = JSON.parse(productCartJon)
            console.log(productCart)
            productCart.map((value, i) => {
                cartTotal += (value.qty * value.price)
            })
        }
        this.setState({
            productQtyAll: productQtyAll,
            cartQty: cartQty,
            productCart: productCart,
            cartTotal: cartTotal
        })
    }

    showProducts(context) {
        let cartTotal = 0

        let { productCart } = this.state
        if (productCart) {
            return productCart.map((value, i) => {
                return (
                    <tr>
                        <td className="cart_product">
                            <a href><img src="images/cart/one.png" alt="" /></a>
                        </td>
                        <td className="cart_description">
                            <h4><a href>{value.name}</a></h4>
                            <p>Web ID: {value.id}</p>
                        </td>
                        <td className="cart_price">
                            <p>${value.price}</p>
                        </td>
                        <td className="cart_quantity">
                            <div className="cart_quantity_button">
                                <a href onClick={() => { this.changeQty(productCart, value.id, 1, context) }} className="cart_quantity_up"> + </a>
                                <input className="cart_quantity_input" type="text" name="quantity" value={value.qty} defaultValue={1} autoComplete="off" size={2} />
                                <a href onClick={() => { this.changeQty(productCart, value.id, -1, context) }} className="cart_quantity_down"> - </a>
                            </div>
                        </td>
                        <td className="cart_total">
                            <p className="cart_total_price">${value.qty * value.price}</p>
                        </td>
                        <td className="cart_delete">
                            <a href onClick={() => { this.deleteProduct(productCart, value.id, context) }} className="cart_quantity_delete"><i className="fa fa-times" /></a>
                        </td>
                    </tr>
                )
            })
        }
    }

    // Tăng giảm số lương sp
    changeQty(productCart, productId, changedValue, context) {
        let productCartChanged = null
        let { cartQty, productQtyAll, cartTotal } = this.state

        // Tăng giảm số lương sp của arr rồi gán vào arr mới
        if (productCart) {
            productCartChanged = productCart.map((value, i) => {
                if (value.id == productId) {
                    cartQty += changedValue
                    value.qty += changedValue

                    if (value.qty == 0) {
                        Object.keys(productQtyAll).map((key, i) => {
                            if (key == productId) {
                                delete productQtyAll[key]
                            }
                        })
                        cartTotal -= (value.price)
                        return null
                    } else {
                        changedValue == 1 ? cartTotal += (value.price) : cartTotal -= (value.price)
                        Object.keys(productQtyAll).map((key, i) => {
                            if (key == productId) {
                                productQtyAll[key] += changedValue
                            }
                        })
                    }
                    return (value)
                }
                return (value)
            })
        }

        productCartChanged.map((value, i) => {
            if (value) {
                // cartTotal += (value.price)
            } else {
                productCartChanged.splice(i, 1)
            }
        })

        context.handleCartQty(cartQty)

        this.setState({
            productCart: productCartChanged,
            cartTotal: cartTotal,
            cartQty: cartQty,
            productQtyAll: productQtyAll
        })

        localStorage.setItem('productQtyAll', JSON.stringify(productQtyAll))
        localStorage.setItem('cartQty', JSON.stringify(cartQty))
        localStorage.setItem('productCart', JSON.stringify(productCartChanged))

    }

    deleteProduct(productCart, productId, context) {
        let { cartQty, productQtyAll, cartTotal } = this.state

        console.log(productQtyAll)
        if (productCart) {
            var productCartDeleted = productCart.map((value, i) => {
                if (value.id == productId) {
                    cartQty = cartQty - value.qty
                    cartTotal = cartTotal - (value.qty * value.price)
                    return null;
                }
                return value
            })
        }

        console.log(cartQty)

        productCartDeleted.map((value, i) => {
            if (value) {
                return value
            } else {
                productCartDeleted.splice(i, 1)
            }
        })

        if (productQtyAll) {
            Object.keys(productQtyAll).map((key, i) => {
                if (key == productId) {
                    delete productQtyAll[key]
                }
            })
        }

        context.handleCartQty(cartQty)

        localStorage.setItem('productQtyAll', JSON.stringify(productQtyAll))
        localStorage.setItem('productCart', JSON.stringify(productCartDeleted))
        localStorage.setItem('cartQty', JSON.stringify(cartQty))

        this.setState({
            productCart: productCartDeleted,
            productQtyAll: productQtyAll,
            cartQty: cartQty,
            cartTotal: cartTotal
        })

    }

    showCartTotal() {
        let { cartTotal } = this.state
        return (
            <li>
                Total
                <span>${cartTotal}</span>
            </li>
        )
    }

    render() {
        return (
            <div>
                <section id="cart_items">
                    <div className="container">
                        <div className="breadcrumbs">
                            <ol className="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li className="active">Shopping Cart</li>
                            </ol>
                        </div>
                        <div className="table-responsive cart_info">
                            <table className="table table-condensed">
                                <thead>
                                    <tr className="cart_menu">
                                        <td className="image">Item</td>
                                        <td className="description" />
                                        <td className="price">Price</td>
                                        <td className="quantity">Quantity</td>
                                        <td className="total">Total</td>
                                        <td />
                                    </tr>
                                </thead>
                                <tbody id="product-cart">
                                    <MyContext.Consumer>
                                        {(context) => (
                                            this.showProducts(context)
                                        )}
                                    </MyContext.Consumer>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="do_action">
                    <div className="container">
                        <div className="heading">
                            <h3>What would you like to do next?</h3>
                            <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
                        </div>
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="chose_area">
                                    <ul className="user_option">
                                        <li>
                                            <input type="checkbox" />
                                            <label>Use Coupon Code</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" />
                                            <label>Use Gift Voucher</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" />
                                            <label>Estimate Shipping &amp; Taxes</label>
                                        </li>
                                    </ul>
                                    <ul className="user_info">
                                        <li className="single_field">
                                            <label>Country:</label>
                                            <select>
                                                <option>United States</option>
                                                <option>Bangladesh</option>
                                                <option>UK</option>
                                                <option>India</option>
                                                <option>Pakistan</option>
                                                <option>Ucrane</option>
                                                <option>Canada</option>
                                                <option>Dubai</option>
                                            </select>
                                        </li>
                                        <li className="single_field">
                                            <label>Region / State:</label>
                                            <select>
                                                <option>Select</option>
                                                <option>Dhaka</option>
                                                <option>London</option>
                                                <option>Dillih</option>
                                                <option>Lahore</option>
                                                <option>Alaska</option>
                                                <option>Canada</option>
                                                <option>Dubai</option>
                                            </select>
                                        </li>
                                        <li className="single_field zip-field">
                                            <label>Zip Code:</label>
                                            <input type="text" />
                                        </li>
                                    </ul>
                                    <a className="btn btn-default update" href>Get Quotes</a>
                                    <a className="btn btn-default check_out" href>Continue</a>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="total_area">
                                    <ul>
                                        <li>Cart Sub Total <span>$59</span></li>
                                        <li>Eco Tax <span>$2</span></li>
                                        <li>Shipping Cost <span>Free</span></li>
                                        {this.showCartTotal()}
                                    </ul>
                                    <a className="btn btn-default update" href>Update</a>
                                    <a className="btn btn-default check_out" href>Check Out</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Cart;
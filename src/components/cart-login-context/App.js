import React, { Component } from "react";
import Header from './components/Layout/Header'
import MenuLeft from './components/Layout/MenuLeft'
import Footer from './components/Layout/Footer'
import {
	withRouter
} from 'react-router-dom';
import {MyProvider} from "./components/MyProvider";

class App extends Component {
	constructor(props) {
		super(props)
		
	}

	// Kiểm tra xem có đang ở Component Account ko để Ẩn/Hiện MenuLeft
	showMenuLeft(propsValue) {
		// console.log(propsValue.location.pathname)
		let url = propsValue.location.pathname

		if (url.indexOf('account') !== -1 || url.indexOf('cart') !== -1) {
			return (
				<div>
					{this.props.children}
				</div>
			)
		} else {
			return (
				<div>
					<MenuLeft />
					{this.props.children}
				</div>
			)
		}
	}

	getIsLoginValue() {

	}

	render() {
		// console.log(this.props)

		return (
			<MyProvider>
				<div className="App">
					<Header />
					<section>
						<div className='container'>
							<div className='row'>
								{this.showMenuLeft(this.props)}
								{/* <MenuLeft />
							{this.props.children} */}
							</div>
						</div>
					</section>
					<Footer />
				</div>
			</MyProvider>
		);
	}
}

export default withRouter(App);

import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { MyContext } from '.././MyProvider'

class Features_items extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataProducts: [],
            id_user: '',
            cartQty: 0
        }
    }


    componentDidMount() {
        axios({
            method: 'GET',
            url: 'http://localhost/laravel/laravel/public/api/product'
        }).then(res => {
            // console.log(res)
            // console.log(res.data.data)
            this.setState({
                dataProducts: res.data.data
            })
        }).catch(err => {
            console.log(err)
        })

        let userDataJson = localStorage.getItem('userData')
        if (userDataJson) {
            let userData = JSON.parse(userDataJson)
            this.setState({
                id_user: Number(userData.id_user)
            })
        }
    }

    showFeaturesItems(context) {
        let { dataProducts } = this.state
        return dataProducts.map((product, i) => {
            let imgProductJson = product.image
            let imgProduct = JSON.parse(imgProductJson)
            // console.log(imgProduct[0])
            return (
                <div key={i} className="col-sm-4">
                    <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                                <img src={"http://localhost/laravel/laravel/public/upload/user/product/" + product.id_user + "/" + imgProduct[0]} alt="" />
                                <h2>${product.price}</h2>
                                <p>{product.name}</p>
                                <a href className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                            </div>
                            <div className="product-overlay">
                                <Link to={'/product/detail/' + product.id} className='product-more' />
                                <div className="overlay-content">
                                    <h2>${product.price}</h2>
                                    <p>{product.name}</p>
                                    <a href onClick={() => { this.addToCart(product, context) }} className="btn btn-default add-to-cart">
                                        <i className="fa fa-shopping-cart" />
                                        Add to cart
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="choose">
                            <ul className="nav nav-pills nav-justified">
                                <li><a href="#"><i className="fa fa-plus-square" />Add to wishlist</a></li>
                                <li><a href="#"><i className="fa fa-plus-square" />Add to compare</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            )
        })
    }

    addToCart(product, context) {
        let cartQty = 0
        let cartQtyJson = localStorage.getItem('cartQty')
        if (cartQtyJson) {
            cartQty = parseInt(cartQtyJson)
        }

        let productQtyAll = {}
        let productId = product.id
        let qty = 1

        let productQtyAllJson = localStorage.getItem('productQtyAll')
        if (productQtyAllJson) {
            productQtyAll = JSON.parse(productQtyAllJson)
            Object.keys(productQtyAll).map((id, i) => {
                console.log(id)
                if (id == product.id) {
                    qty = productQtyAll[id]
                    ++qty
                }
            })
        }

        let productQty = qty
        productQtyAll[productId] = productQty

        axios({
            method: 'POST',
            url: 'http://localhost/laravel/laravel/public/api/product/cart',
            data: productQtyAll
        }).then(res => {
            console.log(res)
            console.log(res.data.data)
            let productCartJon = JSON.stringify(res.data.data)
            localStorage.setItem('productCart', productCartJon)
            console.log(productCartJon)
        }).catch(err => {
            console.log(err)
        })
        console.log(productQtyAll)

        productQtyAllJson = JSON.stringify(productQtyAll)
        localStorage.setItem('productQtyAll', productQtyAllJson)

        cartQty += 1
        cartQtyJson = JSON.stringify(cartQty)
        localStorage.setItem('cartQty', cartQtyJson)
        console.log(context.handleCartQty)
        context.handleCartQty(cartQty)

        this.setState({
            cartQty: cartQty
        })
    }

    render() {
        // const myContext = userContext(MyContext)
        // console.log(myContext)


        return (
            <div className="features_items">

                <h2 className="title text-center">Features Items</h2>

                <MyContext.Consumer>
                    {(context) => (
                        this.showFeaturesItems(context)
                    )}
                </MyContext.Consumer>

                {/* <div className="col-sm-4">
                    <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/product1.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                            </div>
                            <div className="product-overlay">
                                <div className="overlay-content">
                                    <h2>$56</h2>
                                    <p>Easy Polo Black Edition</p>
                                    <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                                </div>
                            </div>
                        </div>
                        <div className="choose">
                            <ul className="nav nav-pills nav-justified">
                                <li><a href="#"><i className="fa fa-plus-square" />Add to wishlist</a></li>
                                <li><a href="#"><i className="fa fa-plus-square" />Add to compare</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/product2.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                            </div>
                            <div className="product-overlay">
                                <div className="overlay-content">
                                    <h2>$56</h2>
                                    <p>Easy Polo Black Edition</p>
                                    <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                                </div>
                            </div>
                        </div>
                        <div className="choose">
                            <ul className="nav nav-pills nav-justified">
                                <li><a href="#"><i className="fa fa-plus-square" />Add to wishlist</a></li>
                                <li><a href="#"><i className="fa fa-plus-square" />Add to compare</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/product3.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                            </div>
                            <div className="product-overlay">
                                <div className="overlay-content">
                                    <h2>$56</h2>
                                    <p>Easy Polo Black Edition</p>
                                    <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                                </div>
                            </div>
                        </div>
                        <div className="choose">
                            <ul className="nav nav-pills nav-justified">
                                <li><a href="#"><i className="fa fa-plus-square" />Add to wishlist</a></li>
                                <li><a href="#"><i className="fa fa-plus-square" />Add to compare</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/product4.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                            </div>
                            <div className="product-overlay">
                                <div className="overlay-content">
                                    <h2>$56</h2>
                                    <p>Easy Polo Black Edition</p>
                                    <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                                </div>
                            </div>
                            <img src="images/home/new.png" className="new" alt="" />
                        </div>
                        <div className="choose">
                            <ul className="nav nav-pills nav-justified">
                                <li><a href="#"><i className="fa fa-plus-square" />Add to wishlist</a></li>
                                <li><a href="#"><i className="fa fa-plus-square" />Add to compare</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/product5.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                            </div>
                            <div className="product-overlay">
                                <div className="overlay-content">
                                    <h2>$56</h2>
                                    <p>Easy Polo Black Edition</p>
                                    <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                                </div>
                            </div>
                            <img src="images/home/sale.png" className="new" alt="" />
                        </div>
                        <div className="choose">
                            <ul className="nav nav-pills nav-justified">
                                <li><a href="#"><i className="fa fa-plus-square" />Add to wishlist</a></li>
                                <li><a href="#"><i className="fa fa-plus-square" />Add to compare</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="product-image-wrapper">
                        <div className="single-products">
                            <div className="productinfo text-center">
                                <img src="images/home/product6.jpg" alt="" />
                                <h2>$56</h2>
                                <p>Easy Polo Black Edition</p>
                                <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                            </div>
                            <div className="product-overlay">
                                <div className="overlay-content">
                                    <h2>$56</h2>
                                    <p>Easy Polo Black Edition</p>
                                    <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                                </div>
                            </div>
                        </div>
                        <div className="choose">
                            <ul className="nav nav-pills nav-justified">
                                <li><a href="#"><i className="fa fa-plus-square" />Add to wishlist</a></li>
                                <li><a href="#"><i className="fa fa-plus-square" />Add to compare</a></li>
                            </ul>
                        </div>
                    </div>
                </div> */}
            </div>
        );
    }
}

export default Features_items;
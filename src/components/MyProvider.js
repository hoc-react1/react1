import React, {Component} from 'react';
const MyContext = React.createContext();

class MyProvider extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            cartQty: 0,
            isLogin: false,
            handleCartQty: (newCartQty) => this.setState({
                cartQty : newCartQty
            }),
            handleIsLogin: (isLogin) => this.setState({
                isLogin: isLogin
            })
        }
    }
    
    componentDidMount() {
        let cartQtyJson = localStorage.getItem('cartQty')
        let isLoginJson = localStorage.getItem('isLogin')
        if (isLoginJson) {
            this.setState({
                isLogin : JSON.parse(isLoginJson)
            })
        }
        if (cartQtyJson) {
            this.setState({
                cartQty : JSON.parse(cartQtyJson)
            })
        }
    }

    render() {
        return (
            <MyContext.Provider value={this.state}>
                {this.props.children}
            </MyContext.Provider>
        );
    }
}

export { MyProvider, MyContext };
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './App.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {
    BrowserRouter,
    Route
} from 'react-router-dom';
import Home from './components/Home';
import BlogList from './components/Blog/BlogList'
import DetailBlog from './components/Blog/DetailBlog'
import Login from './components/Login/Login'
import Product_detail from './components/Products/Product_detail';
import Cart from './components/Cart/Cart';
import LoginHook from './components/Login/LoginHook'

import Account from './components/Account/Index'

ReactDOM.render(
    <BrowserRouter>
        <App>
            <Route exact path='/' component={Home} />
            <Route exact path='/product/detail/:id' component={Product_detail} />
            <Route exact path='/blog/list' component={BlogList} />
            <Route path='/blog/list/detail/:id' component={DetailBlog} />
            {/* <Route path='/login' component={Login} /> */}
            <Route path='/login' component={LoginHook} />
            <Route path='/account' component={Account} />
            <Route path='/cart' component={Cart} />
        </App>
    </BrowserRouter>,
    document.getElementById('root')
);

reportWebVitals();
